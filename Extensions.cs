﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniDelegheF24BT
{
    public static class Extensions
    {
        public static string ToJson(this object target)
        {
            return JsonConvert.SerializeObject(target, Formatting.Indented);
        }

        public static string ToXml(this object target)
        {
            return JsonConvert.DeserializeXNode(target.ToJson(), "Data").ToString();
        }

        public static object ValueOrDbNull(this object target)
        {
            return target == null ? DBNull.Value : target;
        }
        
        public static string FormatNull(this object target)
        {
            return target == null ? "NULL" : $"'{target}'";
        }
    }
}
