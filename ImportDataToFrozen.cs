﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.OleDb;

namespace UniDelegheF24BT
{
    class ImportDataToFrozen
    {
        private ImportDataToFrozen()
        { }

        public static void Execute()
        {
            var instance = new ImportDataToFrozen();
            instance.ExecuteInternal();
        }

        protected void ExecuteInternal()
        {
            //Leggo Enti bilaterali da SqlServer catalogo ...
            //string query = "SELECT ElencoSediAttive.COD6 as Sede_SAP, tbdCodiceCausale.CODCAUS_F24 as Cod_TRI FROM tbdCodiceCausale CROSS JOIN ElencoSediAttive";
            string query = "select '8400' Sede_SAP,'DM10A' Cod_TRI union select '5100' Sede_SAP,'DM10A' Cod_TRI";

            DbDataReader reader;
            reader = SqlDAL.GetDataReader(query, CommandType.Text);
            DataTable dt = null; dt = new DataTable();
            dt.Load(reader);

            // Imposto il rowCont con lo stesso valore del parametro PageSize per farlo entrare nel ciclo while                                    
            using (dt)
            {
                foreach (DataRow row in dt.Rows) 
                {
                    int rowCount = Settings.Instance.PageSize;
                    int pagePrint = 0;
                    while (rowCount == Settings.Instance.PageSize)
                    {
                        pagePrint++;
                        //leggo tabelle DB2 (SC1T02.CCTBSC7A, TEFT1.DETBDM10) parameter(@Sede_SAP, @COD_TRI)
                        //ritorna rowCount con le righe estratte se sono minori del parametro PageSize esco dal ciclo così interrompo la paginazione
                        FetchTableDB2_CCTBSC7A(row["Sede_SAP"].ToString(), row["Cod_TRI"].ToString(), pagePrint, ref rowCount);
                    }
                }
                SqlDAL.CloseConnection();
            }
        }


        private void FetchTableDB2_CCTBSC7A(string Sede_SAP, string Cod_TRI, int pagePrint, ref int rowCount)
        {
            Program.ProcessLogger.Info($"Leggo Page {pagePrint} - Tabelle SC1T02.CCTBSC7A Sede_SAP: {Sede_SAP}, Cod_TRI: {Cod_TRI}");

            var query = $@"SELECT SC1T02.CCTBSC7A.SC7A_NUM_SC7 FROM SC1T02.CCTBSC7A                           
                        WHERE SC1T02.CCTBSC7A.SC7A_TIP_DOC = 'E2' and SC1T02.CCTBSC7A.SC7A_FLG_READ = 1 AND SC1T02.CCTBSC7A.SC7A_COD_ERR = 0 and SC7A_num_reg in (8,11)
                        and SC1T02.CCTBSC7A.SC7A_COD_SAP = '{Sede_SAP}' 
                        AND SC1T02.CCTBSC7A.SC7A_COD_TRI = '{Cod_TRI}' 
                        FETCH FIRST {Settings.Instance.PageSize} ROW ONLY WITH ur";


            //Program.ProcessLogger.Info($" Query DB = {query }");

            rowCount = 0;
            
            DataTable dt = null;

            dt = PopolaDataTabletDB2(query);
            
            if (dt == null)
            {
                Program.ProcessLogger.Error($"Impossibile recuperare i dati da DB");
            }
            else
            {                
                try
                {
                    using (dt)
                    {

                        SqlConnection connectionSql = new SqlConnection(ConfigurationManager.ConnectionStrings["UniF24"].ConnectionString);                        
                        SqlCommand commandSql = connectionSql.CreateCommand();                        
                        SqlTransaction transactionSql;

                        OleDbConnection connectionDb2 = new OleDbConnection("Provider=DB2OLEDB;Persist Security Info=False; Initial Catalog=A01DB2.TEFT1;Defer Prepare= FALSE;Network Transport Library=TCPIP;Host CCSID=20280;Max Pool Size=10;Connect Timeout=15;Network Address=svil.host.inps;Network Port=5025;Package Collection=COLDASV;Connection Pooling=True; Client Application Name=NomeApplicazione; Auto Commit=True;Units of Work=RUW;DBMS Platform=DB2MVS;Authentication=Server; Process Binary as Character=False;Use Early Metadata=False;Derive Parameters=False;Extended Properties=&quot;DBPROP_COMMANDTIMEOUT=1&quot;User Id=WBS00050;Password=Svi96der");
                        

                        foreach (DataRow row in dt.Rows)
                        {
                            bool errCaricamentoSC7 = false;

                            connectionSql.Open();
                            commandSql.Connection = connectionSql;
                            transactionSql = connectionSql.BeginTransaction();
                            commandSql.Transaction = transactionSql;

                            try
                            {
                                int rowCount_DETBDM10 = Settings.Instance.PageSize;
                                int pagePrint_DETBDM10 = 0;
                                int TotRecordLetti_SC7_DETBDM10 = 0;
                                int iRecordInseriti = 0;
                                int DM10_PRG_DET = 0;
                                while (rowCount_DETBDM10 == Settings.Instance.PageSize)
                                {
                                    pagePrint_DETBDM10++;
                                    //leggo tabelle DB2 TEFT1.DETBDM10 parameter(row["SC7A_NUM_SC7"], @Sede_SAP, @COD_TRI)
                                    //ritorna rowCount con le righe estratte se sono minori del parametro PageSize esco dal ciclo così interrompo la paginazione
                                    FetchTableDB2_DETBDM10(Sede_SAP.ToString(), Cod_TRI.ToString(), (decimal)row["SC7A_NUM_SC7"], pagePrint_DETBDM10,ref DM10_PRG_DET, ref iRecordInseriti, ref rowCount_DETBDM10, ref errCaricamentoSC7, commandSql, connectionSql);
                                    TotRecordLetti_SC7_DETBDM10 = TotRecordLetti_SC7_DETBDM10 + rowCount_DETBDM10;
                                }

                                Program.ProcessLogger.Info($"");
                                Program.ProcessLogger.Info($"*** Inserimento  (Sede_SAP = {Sede_SAP}   Cod_TRI = {Cod_TRI}  SC7: FrozenDeleghe = {row["SC7A_NUM_SC7"]}) ****");
                                Program.ProcessLogger.Info($"");
                                Program.ProcessLogger.Info($"Inizio inserimento FrozenDeleghe {TotRecordLetti_SC7_DETBDM10} record recuperati");

                                connectionDb2.Open();
                                OleDbCommand commandDb2 = connectionDb2.CreateCommand();
                                commandDb2.Connection = connectionDb2;
                                OleDbTransaction transactionDb2;
                                transactionDb2 = connectionDb2.BeginTransaction();
                                commandDb2.Transaction = transactionDb2;

                                if (errCaricamentoSC7)
                                {
                                    transactionSql.Rollback();                                    
                                    Program.ProcessLogger.Error($"Errore Popolazione Sede_SAP = {Sede_SAP}   Cod_TRI = {Cod_TRI}  SC7: FrozenDeleghe = {row["SC7A_NUM_SC7"]}");                                    
                                }
                                else
                                {
                                    try
                                    {
                                        var queryDb2 = $@"update SC1T02.CCTBSC7A set SC7A_FLG_READ = 5
                                                    where SC7A_NUM_SC7 = {row["SC7A_NUM_SC7"]}
                                                    and SC7A_COD_SAP = {Sede_SAP} 
                                                    and SC7A_COD_TRI = '{Cod_TRI}'
                                                    ";
                                        DB2DAL.ExecuteNonQueryTrans(queryDb2, CommandType.Text, commandDb2, connectionDb2);

                                        transactionSql.Commit();
                                        transactionDb2.Commit();
                                        Program.ProcessLogger.Info($"Inserimento {iRecordInseriti} record completato");                                                                                
                                    }
                                    catch (Exception ex)
                                    {
                                        transactionSql.Rollback();
                                        transactionDb2.Rollback();
                                        Program.ProcessLogger.Error(ex, $"Rollback - Errore Popolazione SC7: FrozenDeleghe = {row["SC7A_NUM_SC7"]}");
                                    }                                    
                                }
                            }
                            catch (Exception ex)
                            {
                                transactionSql.Rollback();                                
                                Program.ProcessLogger.Error(ex, $"Rollback - Errore Popolazione SC7: FrozenDeleghe = {row["SC7A_NUM_SC7"]}");
                            }
                            finally
                            {
                                if (connectionSql.State == ConnectionState.Open) { connectionSql.Close(); }
                                if (connectionDb2.State == ConnectionState.Open) { connectionDb2.Close(); }
                                Program.ProcessLogger.Info($"");
                                Program.ProcessLogger.Info($"**********************************************");
                            }
                        }

                    }
                }
                catch (Exception ex)
                {
                    Program.ProcessLogger.Error(ex, $"Errore Ciclo Sede_SAP = {Sede_SAP}   Cod_TRI = {Cod_TRI} ");
                }
                finally
                {
                    rowCount = dt.Rows.Count;                   
                }
            }
        }



        private void FetchTableDB2_DETBDM10(string Sede_SAP, string Cod_TRI, decimal SC7A_NUM_SC7, int pagePrint_DETBDM10, ref int DM10_PRG_DET, ref int iRecordInseriti, ref int rowCount_DETBDM10, ref bool errCaricamentoSC7, SqlCommand commandSql, SqlConnection connectionSql)
        {
            Program.ProcessLogger.Info($"Leggo Page {pagePrint_DETBDM10} - Tabelle TEFT1.DETBDM10,TEFT1.DETBDM10 Sede_SAP: {Sede_SAP}, Cod_TRI: {Cod_TRI}, SC7: SC7A_NUM_SC7 ");

            var query = $@"SELECT SC1T02.CCTBSC7A.SC7A_NUM_SC7, TEFT1.DETBDM10.DM10_PROGR, TEFT1.DETBDM10.DM10_PRG_DET FROM SC1T02.CCTBSC7A 
                            INNER JOIN TEFT1.DETBDM10  ON SC1T02.CCTBSC7A.SC7A_COD_SAP = TEFT1.DETBDM10.DM10_COD_SAP_F24 
                            AND SC1T02.CCTBSC7A.SC7A_NUM_SC7 = TEFT1.DETBDM10.DM10_NUM_SC7 
                        WHERE SC1T02.CCTBSC7A.SC7A_TIP_DOC = 'E2' and SC1T02.CCTBSC7A.SC7A_FLG_READ = 1 AND SC1T02.CCTBSC7A.SC7A_COD_ERR = 0 and SC7A_num_reg in (8,11)
                        and SC1T02.CCTBSC7A.SC7A_COD_SAP = '{Sede_SAP}' 
                        AND SC1T02.CCTBSC7A.SC7A_COD_TRI = '{Cod_TRI}' 
                        AND SC1T02.CCTBSC7A.SC7A_NUM_SC7 = {SC7A_NUM_SC7}
                        AND TEFT1.DETBDM10.DM10_PRG_DET > {DM10_PRG_DET}
                        order by TEFT1.DETBDM10.DM10_PRG_DET
                        FETCH FIRST {Settings.Instance.PageSize} ROW ONLY WITH ur";

            //Program.ProcessLogger.Info($" Query DB = {query }");

            rowCount_DETBDM10 = 0;
           
            DataTable dt = null;

            dt = PopolaDataTabletDB2(query);

            if (dt == null)
            {
                Program.ProcessLogger.Error($"FetchTableDB2_DETBDM10: Impossibile recuperare i dati da DB");
            }
            else
            {               
                try
                {
                    using (dt)
                    {                        
                        var querySql = "";
                        var queryDb2 = "";
                        try
                        {
                            foreach (DataRow row in dt.Rows)
                            {                               
                                try
                                {
                                    DM10_PRG_DET = (int)row["DM10_PRG_DET"];
                                    querySql = $@"INSERT INTO FrozenDeleghe (DM10_NUM_SC7,FLG_READ) VALUES ({row["SC7A_NUM_SC7"]},{Sede_SAP})";

                                    SqlDAL.ExecuteNonQueryTrans(querySql, CommandType.Text, commandSql, connectionSql);

                                    iRecordInseriti++;
                                }
                                catch (Exception ex)
                                {                                  
                                    Program.ProcessLogger.Error(ex, $"Errore inserimento FrozenDeleghe = {querySql}   Aggiornamento SC1T02.CCTBSC7A =  {queryDb2}");
                                }
                            }                            
                        }
                        catch (Exception ex)
                        {
                            errCaricamentoSC7 = true;
                            Program.ProcessLogger.Error(ex, $"Errore Ciclo Sede_SAP = {Sede_SAP} , Cod_TRI = {Cod_TRI}");
                        }
                        finally
                        {
                            
                            rowCount_DETBDM10 = dt.Rows.Count;                                               
                        }
                    }
                }
                catch (Exception ex)
                {
                    Program.ProcessLogger.Warn(ex, $"Errore inserimento FrozenDeleghe {query}");
                }
            }
        }

        private static DataTable PopolaDataTabletDB2(string query)
        {
           
            //Program.ProcessLogger.Info($" Query DB = {query }");

            DataTable dt = null;
            var attempt = 0;

            //"Recupero dati da DB2, tentativo {0} di {1}"
            while (attempt++ < Settings.Instance.MaxAttempts)
            {
                //Program.ProcessLogger.Info($"Recupero dati da DB2, tentativo {attempt} di {Settings.Instance.MaxAttempts}");
                DbDataReader reader = null;
                var canceller = new CancellationTokenSource();

                var task = Task.Run(() =>
                {
                    try
                    {
                        using (canceller.Token.Register(Thread.CurrentThread.Abort))
                            reader = DB2DAL.GetDataReader(query, CommandType.Text);
                    }
                    catch
                    {
                        for (var i = 0; i < 5; i++)
                        {
                            try
                            {
                                DB2DAL.CloseConnection();
                            }
                            catch
                            { }
                        }
                    }
                });

                if (task.Wait((int)Settings.Instance.Timeout.TotalMilliseconds, canceller.Token))
                {
                    try
                    {
                        dt = new DataTable();
                        using (reader)
                            dt.Load(reader);
                        break;
                    }
                    catch (Exception ex)
                    {
                        Program.ProcessLogger.Warn(ex, $"Tentativo {attempt} di {Settings.Instance.MaxAttempts} fallito");
                        continue;
                    }
                }

                canceller.Cancel();
                //Program.ProcessLogger.Warn($"Tentativo {attempt} di {Settings.Instance.MaxAttempts} fallito");
            }

            return dt;
        }





        private void FetchTableDB2(string Sede_SAP, string Cod_TRI, int pagePrint, ref int rowCount, ref bool errConnection)
        {
            Program.ProcessLogger.Info($"Leggo Page {pagePrint} - Tabelle SC1T02.CCTBSC7A,TEFT1.DETBDM10 Sede_SAP: {Sede_SAP}, Cod_TRI: {Cod_TRI}");

            var query = $@"SELECT SC1T02.CCTBSC7A.SC7A_NUM_SC7, TEFT1.DETBDM10.DM10_PROGR, TEFT1.DETBDM10.DM10_PRG_DET FROM SC1T02.CCTBSC7A 
                            INNER JOIN TEFT1.DETBDM10  ON SC1T02.CCTBSC7A.SC7A_COD_SAP = TEFT1.DETBDM10.DM10_COD_SAP_F24 
                            AND SC1T02.CCTBSC7A.SC7A_NUM_SC7 = TEFT1.DETBDM10.DM10_NUM_SC7 
                        WHERE SC1T02.CCTBSC7A.SC7A_TIP_DOC = 'E2' and SC1T02.CCTBSC7A.SC7A_FLG_READ = 1 AND SC1T02.CCTBSC7A.SC7A_COD_ERR = 0 and SC7A_num_reg in (8,11)
                        and SC1T02.CCTBSC7A.SC7A_COD_SAP = '{Sede_SAP}' 
                        AND SC1T02.CCTBSC7A.SC7A_COD_TRI = '{Cod_TRI}' 
                        FETCH FIRST {Settings.Instance.PageSize} ROW ONLY WITH ur";
            

            //Program.ProcessLogger.Info($" Query DB = {query }");
            
            rowCount = 0;
            errConnection = false;

            DataTable dt = null;
            var attempt = 0;

            //"Recupero dati da DB2, tentativo {0} di {1}"
            while (attempt++ < Settings.Instance.MaxAttempts)
            {
                //Program.ProcessLogger.Info($"Recupero dati da DB2, tentativo {attempt} di {Settings.Instance.MaxAttempts}");
                DbDataReader reader = null;
                var canceller = new CancellationTokenSource();

                var task = Task.Run(() =>
                {
                    try
                    {
                        using (canceller.Token.Register(Thread.CurrentThread.Abort))
                            reader = DB2DAL.GetDataReader(query, CommandType.Text);
                    }
                    catch
                    {
                        for (var i = 0; i < 5; i++)
                        {
                            try
                            {
                                DB2DAL.CloseConnection();
                            }
                            catch
                            { }
                        }
                    }
                });

                if (task.Wait((int)Settings.Instance.Timeout.TotalMilliseconds, canceller.Token))
                {
                    try
                    {
                        dt = new DataTable();
                        using (reader)                            
                            dt.Load(reader);                        
                        break;
                    }
                    catch (Exception ex)
                    {
                        Program.ProcessLogger.Warn(ex, $"Tentativo {attempt} di {Settings.Instance.MaxAttempts} fallito");
                        continue;
                    }
                }

                canceller.Cancel();
                Program.ProcessLogger.Warn($"Tentativo {attempt} di {Settings.Instance.MaxAttempts} fallito");
            }

            if (dt == null)
            {
                Program.ProcessLogger.Error($"Impossibile recuperare i dati da DB");                
            }
            else
            {
                Program.ProcessLogger.Info($"Inizio inserimento {dt.Rows.Count} record recuperati");         
                try
                {                   
                    using (dt)
                    {
                       
                        SqlConnection connectionSql = new SqlConnection(ConfigurationManager.ConnectionStrings["UniF24"].ConnectionString);
                        connectionSql.Open();
                        SqlCommand commandSql = connectionSql.CreateCommand();                        
                        commandSql.Connection = connectionSql;
                        SqlTransaction transactionSql;

                        OleDbConnection connectionDb2 = new OleDbConnection("Provider=DB2OLEDB;Persist Security Info=False; Initial Catalog=A01DB2.TEFT1;Defer Prepare= FALSE;Network Transport Library=TCPIP;Host CCSID=20280;Max Pool Size=10;Connect Timeout=15;Network Address=svil.host.inps;Network Port=5025;Package Collection=COLDASV;Connection Pooling=True; Client Application Name=NomeApplicazione; Auto Commit=True;Units of Work=RUW;DBMS Platform=DB2MVS;Authentication=Server; Process Binary as Character=False;Use Early Metadata=False;Derive Parameters=False;Extended Properties=&quot;DBPROP_COMMANDTIMEOUT=1&quot;User Id=WBS00050;Password=Svi96der");
                        connectionDb2.Open();
                        OleDbCommand commandDb2 = connectionDb2.CreateCommand();                        
                        commandDb2.Connection = connectionDb2;
                        OleDbTransaction transactionDb2;
                       
                        int iRecordInseriti = 0;
                        var querySql = "";
                        var queryDb2 = "";
                        try
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                transactionSql = connectionSql.BeginTransaction();
                                commandSql.Transaction = transactionSql;

                                transactionDb2 = connectionDb2.BeginTransaction();
                                commandDb2.Transaction = transactionDb2;
                                try
                                {                                  
                                    querySql = $@"INSERT INTO FrozenDeleghe (DM10_NUM_SC7,FLG_READ) VALUES ({row["SC7A_NUM_SC7"]},{Sede_SAP})";
                            
                                    SqlDAL.ExecuteNonQueryTrans(querySql, CommandType.Text, commandSql, connectionSql);

                                    queryDb2 = $@"update SC1T02.CCTBSC7A set SC7A_FLG_READ = 5
                                                    where SC7A_NUM_SC7 = {row["SC7A_NUM_SC7"]}
and SC7A_COD_SAP = {Sede_SAP} 
and SC7A_COD_TRI = '{Cod_TRI}'
";
                                    DB2DAL.ExecuteNonQueryTrans(queryDb2, CommandType.Text, commandDb2, connectionDb2);

                                    transactionSql.Commit();
                                    transactionDb2.Commit();
                                    iRecordInseriti++;
                                }
                                catch (Exception ex)
                                {
                                    transactionSql.Rollback();
                                    transactionDb2.Rollback();
                                    Program.ProcessLogger.Error(ex, $"Errore inserimento FrozenDeleghe = {querySql}   Aggiornamento SC1T02.CCTBSC7A =  {queryDb2}");
                                }
                            }                            
                            Program.ProcessLogger.Info($"Inserimento {iRecordInseriti} record completato");
                        }
                        catch (Exception ex)
                        {
                            Program.ProcessLogger.Error(ex, $"Errore Ciclo Sede_SAP = {Sede_SAP}   Cod_TRI = {Cod_TRI}");
                        }
                        finally
                        {
                            rowCount = dt.Rows.Count;
                            if (connectionSql.State == ConnectionState.Open){ connectionSql.Close();}
                            if (connectionDb2.State == ConnectionState.Open) { connectionDb2.Close();}                            
                        }
                    }
                }
                catch (Exception ex)
                {                  
                    Program.ProcessLogger.Warn(ex, $"Errore inserimento FrozenDeleghe {query}");                   
                }               
            }           
        }
    }
}
