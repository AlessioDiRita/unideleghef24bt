﻿using NLog;
using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniDelegheF24BT
{
    class Program
    {
        public static Logger ApplicationLogger = LogManager.GetLogger("ApplicationLogger");
        public static Logger ProcessLogger = LogManager.GetLogger("ProcessLogger");

        static int Main(string[] args)
        {
            var executionId = Guid.NewGuid();
            ApplicationLogger.SetProperty("ExecutionId", executionId);
            ProcessLogger.SetProperty("ExecutionId", executionId);

            try
            {
                ApplicationLogger.Info($"UniF24BT versione: {Assembly.GetEntryAssembly().GetName().Version}");
                ApplicationLogger.Debug(File.ReadAllText($"{Path.GetFileName(Assembly.GetExecutingAssembly().Location)}.config"));

                ProcessLogger.Info(Settings.Instance, "Parametri di lancio");

                if (Settings.Instance.Debug)
                    LogManager.Configuration.FindRuleByName("ProcessLogger")?.EnableLoggingForLevel(LogLevel.Debug);
                if (Settings.Instance.TraceLevel != TraceLevel.Disabled)
                    LogManager.Configuration.FindRuleByName("ProcessLogger")?.EnableLoggingForLevel(LogLevel.Trace);

                LogManager.ReconfigExistingLoggers();

                if (!Settings.Instance.MainSwitch)
                    ProcessLogger.Info("UniDelegheF24BT Disattivato");
                else
                {
                    ImportDataToFrozen.Execute();
                }

#if DEBUG
                Console.ReadLine();
#endif
                ProcessLogger.Info("Fine Procedura");
                return 0;
            }
            catch (Exception ex)
            {
                ApplicationLogger.Fatal(ex, ex.Message);
#if DEBUG
                Console.ReadLine();
#endif
                return 2;
            }
        }
    }    
}
