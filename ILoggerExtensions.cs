﻿using System;
using UniDelegheF24BT;

namespace NLog
{
    public static class ILoggerExtensions
    {
        public static void Info(this ILogger target, object data, string message) => Info(target, data, null, message);

        public static void Info(this ILogger target, object data, Exception exception, string message) => Log(target, LogLevel.Info, data, exception, message);

        public static void Fatal(this ILogger target, object data, string message) => Fatal(target, data, null, message);

        public static void Fatal(this ILogger target, object data, Exception exception, string message) => Log(target, LogLevel.Fatal, data, exception, message);

        public static void Error(this ILogger target, object data, string message) => Error(target, data, null, message);

        public static void Error(this ILogger target, object data, Exception exception, string message) => Log(target, LogLevel.Error, data, exception, message);

        public static void Warn(this ILogger target, object data, string message) => Warn(target, data, null, message);

        public static void Warn(this ILogger target, object data, Exception exception, string message) => Log(target, LogLevel.Warn, data, exception, message);

        public static void Debug(this ILogger target, object data, string message) => Debug(target, data, null, message);

        public static void Debug(this ILogger target, object data, Exception exception, string message) => Log(target, LogLevel.Debug, data, exception, message);

        public static void Trace(this ILogger target, object data, string message) => Trace(target, data, null, message);

        public static void Trace(this ILogger target, object data, Exception exception, string message) => Log(target, LogLevel.Trace, data, exception, message);

        public static void Log(this ILogger target, LogLevel level, object data, Exception exception, string message)
        {
            var item = new LogEventInfo(level, null, null, message, null, exception);

            item.Properties["Data"] = data.ToXml();

            target.Log(item);
        }
    }
}
