﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UniDelegheF24BT
{
    public static class DB2DAL
    {
        //ConfigurationManager.ConnectionStrings["DB2"].ConnectionString);
        private static OleDbConnection _connection = new OleDbConnection("Provider=DB2OLEDB;Persist Security Info=False; Initial Catalog=A01DB2.TEFT1;Defer Prepare= FALSE;Network Transport Library=TCPIP;Host CCSID=20280;Max Pool Size=10;Connect Timeout=15;Network Address=svil.host.inps;Network Port=5025;Package Collection=COLDASV;Connection Pooling=True; Client Application Name=NomeApplicazione; Auto Commit=True;Units of Work=RUW;DBMS Platform=DB2MVS;Authentication=Server; Process Binary as Character=False;Use Early Metadata=False;Derive Parameters=False;Extended Properties=&quot;DBPROP_COMMANDTIMEOUT=1&quot;User Id=WBS00050;Password=Svi96der");


        public static void CloseConnection() => _connection.Close();

        public static OleDbDataReader GetDataReader(string commandText, CommandType commandType, params OleDbParameter[] parameters)
        {
            var cmd = new OleDbCommand(commandText, _connection) { CommandType = commandType };

            cmd.Parameters.AddRange(parameters);

            lock (_connection)
            {
                _connection.Open();
                return cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
        }

        public static int ExecuteNonQuery(string commandText, CommandType commandType, params OleDbParameter[] parameters)
        {
            var manageConnection = true;
            var cmd = new OleDbCommand(commandText, _connection) { CommandType = commandType };

            cmd.Parameters.AddRange(parameters);

            lock (_connection)
            {
                try
                {
                    manageConnection = _connection.State == ConnectionState.Closed;
                    if (manageConnection)
                        _connection.Open();

                    return cmd.ExecuteNonQuery();
                }
                finally
                {
                    if (manageConnection)
                        _connection.Close();
                }
            }
        }


        public static int ExecuteNonQueryTrans(string commandText, CommandType commandType, OleDbCommand cmd, OleDbConnection _connection, params OleDbParameter[] parameters)
        {           
            cmd.CommandText = commandText;

            cmd.Parameters.AddRange(parameters);

            lock (_connection)
            {               
                return cmd.ExecuteNonQuery();               
            }
        }

        //public static OleDbDataReader GetDataReader(int maxAttempts, string beginMessage, string commandText, CommandType commandType, params OleDbParameter[] parameters)
        //{
        //    DataTable dt = null;
        //    var attempt = 0;

        //    while (attempt++ < Settings.Instance.MaxAttempts)
        //    {
        //        Program.ProcessLogger.Info(string.Format(beginMessage, attempt, maxAttempts));
        //        OleDbDataReader reader = null;
        //        var canceller = new CancellationTokenSource();

        //        var task = Task.Run(() =>
        //        {
        //            try
        //            {
        //                using (canceller.Token.Register(Thread.CurrentThread.Abort))
        //                    reader = DB2DAL.GetDataReader(commandText, commandType, parameters);
        //            }
        //            catch
        //            {
        //                for (var i = 0; i < 5; i++)
        //                {
        //                    try
        //                    {
        //                        DB2DAL.CloseConnection();
        //                    }
        //                    catch
        //                    { }
        //                }
        //            }
        //        });

        //        if (task.Wait((int)Settings.Instance.Timeout.TotalMilliseconds, canceller.Token))
        //        {
        //            try
        //            {
        //                dt = new DataTable();
        //                using (reader)
        //                    dt.Load(reader);

        //                break;
        //            }
        //            catch (Exception ex)
        //            {
        //                Program.ProcessLogger.Warn(ex, $"Tentativo {attempt} di {Settings.Instance.MaxAttempts} fallito");
        //                continue;
        //            }
        //        }

        //        canceller.Cancel();
        //        Program.ProcessLogger.Warn($"Tentativo {attempt} di {Settings.Instance.MaxAttempts} fallito");
        //    }

        //    return dt;
        //}

        public static void ElaborateResultset(Action<OleDbDataReader> action, string commandText, CommandType commandType, params OleDbParameter[] parameters)
        {
            using (var reader = GetDataReader(commandText, commandType, parameters))
            {
                while (reader.Read())
                {
                    action(reader);
                }
            }
        }
    }
}
