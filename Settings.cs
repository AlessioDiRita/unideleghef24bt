﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniDelegheF24BT
{
    public class Settings
    {
        static Settings()
        {
            SqlDAL.ElaborateResultset((r) =>
            {
                Instance = new Settings()
                {
                    MainSwitch = (bool)r["MainSwitch"],
                    //Ambiente = (String)r["Ambiente"],
                    PageSize = (int)r["RecordsetPageSize"],
                    Debug = (bool)r["Debug"],
                    TraceLevel = (TraceLevel)(byte)r["LivelloTrace"],
                    Timeout = TimeSpan.FromMilliseconds((int)r["Timeout"]),
                    MaxAttempts = (byte)r["TentativiMassimi"],                   
                };
            }
            , "RecuperaParametri", System.Data.CommandType.StoredProcedure);
        }

        public static Settings Instance { get; private set; }

        public bool MainSwitch { get; private set; }

        public string Ambiente { get; private set; }

        public Sources Sources { get; private set; }

        public int PageSize { get; private set; }

        public bool Debug { get; private set; }

        public TraceLevel TraceLevel { get; private set; }

        public TimeSpan Timeout { get; private set; }

        public int MaxAttempts { get; private set; }

        public string DB2Schema => ConfigurationManager.AppSettings["DB2Schema"];

        public string UniF24ConnectionString => ConfigurationManager.ConnectionStrings["UniF24"].ConnectionString;

        public string DB2ConnectionString => ConfigurationManager.ConnectionStrings["DB2"].ConnectionString;
    }

    [Flags]
    public enum Sources
    {
        Unknown = 0,
        Incremental = 1,
        Manual = 2
    }

    [Flags]
    public enum TraceLevel
    {
        Disabled = 0,
        Default = 1,
        Insert = 2
    }
}
