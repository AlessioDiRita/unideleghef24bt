﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Data.Common
{
    public static class DataReaderExtensions
    {
        public static T GetFieldValue<T>(this DbDataReader target, string fieldName) => target.GetFieldValue<T>(target.GetOrdinal(fieldName));

        public static bool IsDBNull(this DbDataReader target, string fieldName) => target.IsDBNull(target.GetOrdinal(fieldName));
    }
}
