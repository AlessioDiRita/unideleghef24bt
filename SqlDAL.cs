﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniDelegheF24BT
{
    public static class SqlDAL
    {
        private static SqlConnection _connection = new SqlConnection(ConfigurationManager.ConnectionStrings["UniF24"].ConnectionString);

        public static SqlDataReader GetDataReader(string commandText, CommandType commandType, params SqlParameter[] parameters)
        {
            var cmd = new SqlCommand(commandText, _connection) { CommandType = commandType };

            cmd.Parameters.AddRange(parameters);

            lock (_connection)
            {
                _connection.Open();
                return cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
        }

        public static void OpenConnection() => _connection.Open();

        public static void CloseConnection() => _connection.Close();

        public static int ExecuteNonQuery(string commandText, CommandType commandType, params SqlParameter[] parameters)
        {
            var manageConnection = true;
            var cmd = new SqlCommand(commandText, _connection) { CommandType = commandType };

            cmd.Parameters.AddRange(parameters);

            lock (_connection)
            {
                try
                {
                    manageConnection = _connection.State == ConnectionState.Closed;
                    if (manageConnection)
                        _connection.Open();

                    return cmd.ExecuteNonQuery();
                }
                finally
                {
                    if (manageConnection)
                        _connection.Close();
                }
            }
        }

        public static int ExecuteNonQueryTrans(string commandText, CommandType commandType, SqlCommand cmd, SqlConnection _connection, params SqlParameter[] parameters)
        {
            cmd.CommandText = commandText;

            cmd.Parameters.AddRange(parameters);

            lock (_connection)
            {
                return cmd.ExecuteNonQuery();
            }
        }

        public static T ExecuteScalar<T>(string commandText, CommandType commandType, params SqlParameter[] parameters)
        {
            var manageConnection = true;
            var cmd = new SqlCommand(commandText, _connection) { CommandType = commandType };

            cmd.Parameters.AddRange(parameters);

            lock (_connection)
            {
                try
                {
                    manageConnection = _connection.State == ConnectionState.Closed;
                    if (manageConnection)
                        _connection.Open();

                    return (T)cmd.ExecuteScalar();
                }
                finally
                {
                    if (manageConnection)
                        _connection.Close();
                }
            }
        }

        public static void ElaborateResultset(Action<SqlDataReader> action, string commandText, CommandType commandType, params SqlParameter[] parameters)
        {
            using (var reader = GetDataReader(commandText, commandType, parameters))
            {
                while (reader.Read())
                {
                    action(reader);
                }
            }
        }

        public static void BulkInsert(DataTable source, string destination)
        {
            using (var bulk = new SqlBulkCopy(ConfigurationManager.ConnectionStrings["UniF24"].ConnectionString,
                SqlBulkCopyOptions.TableLock | SqlBulkCopyOptions.UseInternalTransaction | SqlBulkCopyOptions.KeepNulls)
            { DestinationTableName = destination })
            {
                foreach (DataColumn c in source.Columns)
                {
                    bulk.ColumnMappings.Add(c.ColumnName, c.ColumnName== "IDLOTTO" ? "IdLotto" : c.ColumnName);
                }
                SqlBulkCopyColumnMapping[] a = new SqlBulkCopyColumnMapping[bulk.ColumnMappings.Count];
                bulk.ColumnMappings.CopyTo(a, 0);
                var b = string.Join(Environment.NewLine,from m in a select $"{m.SourceColumn} -> {m.DestinationColumn}");
                bulk.WriteToServer(source);
            }
        }
    }
}
